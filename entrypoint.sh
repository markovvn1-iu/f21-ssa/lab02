#!/bin/bash

# Run this in docker only
if [ ! $(cat /proc/1/cgroup|grep cpu|grep docker|head -n 1) ]; then echo "Run this in Docker only"; exit; fi

FLASK_APP=quoter flask run --host=0.0.0.0