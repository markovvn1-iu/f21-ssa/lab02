from random_word import RandomWords
from quote import quote


class QuoterModel:
    _random_words: RandomWords
    _cur_quote: str
    _cur_quote_author: str

    def __init__(self):
        self._random_words = RandomWords()
        self.next_quote()

    def next_quote(self):
        while True:
            w = self._random_words.get_random_word()
            res = quote(w, limit=1)
            if res:
                self._cur_quote = res[0]['quote']
                self._cur_quote_author = res[0]['author']
                break

    def get_quote(self) -> str:
        return self._cur_quote

    def get_quote_author(self) -> str:
        return self._cur_quote_author

model = QuoterModel()
