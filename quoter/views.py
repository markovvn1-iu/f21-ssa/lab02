from collections import namedtuple
from flask import render_template, request, redirect, url_for

from .app import app
from .model import model


@app.route("/")
def index():  # pragma: no cover
    # state = parse_state(request.args.get("state", "active"))
    # search = request.args.get("search", "")
    # todo_list = filter_todo_items(model, search, state).items()[::-1]
    # number_of_pages = nitems2npages(len(todo_list), ITEMS_PER_PAGE)
    # page = parse_page(request.args.get("page", "0"), number_of_pages)

    return render_template(
        "index.html",
        quote_text=model.get_quote(),
        quote_author=model.get_quote_author(),
    )

@app.route("/api/get_new_quote")
def get_new_quote():  # pragma: no cover
    model.next_quote()
    # model.mark_as_done(int(request.args.get("item_id")))
    return redirect(url_for("index"))
