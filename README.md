# Quoter

This is a solution of a 3rd problem from lab 02 on SSA (Innopolis University). A single screen web app to display a quote on clicking a “Get Quote” button. Currently only Linux is supported.

![](.gitlab/demo_image.png)

### Features

1. Generate random quotes on clicking 'Get new quote'

### Installation (for linux):

1. Install [docker](https://docs.docker.com/get-docker/) and [docker-compose](https://docs.docker.com/compose/install/)
2. Clone the repository: `git clone https://gitlab.com/markovvn1-iu/f21-ssa/lab02.git`
3. cd to lab02 folder: `cd lab02`
4. Build docker image: `make docker_build`
5. Start the container: `make docker_up`
6. Goto website: [http://localhost:5000/](http://localhost:5000/) and have fun!

### Run local (for contributing):

1. Install python >3.8
2. Install all needed dependencies: `make venv`
3. Run locally: `make up`

## Documentation

### How does it work?

When you clock the button 'Get new quote', the request is sent to the server. Then server generates a new random word and then tries to find a quote that contains this word. In case of success current quote has been updated and you are redirected to the main page with the new quota.

```python
from random_word import RandomWords
from quote import quote

def next_quote(self):
    while True:
        w = self._random_words.get_random_word()
        res = quote(w, limit=1)
        if res:
            self._cur_quote = res[0]['quote']
            self._cur_quote_author = res[0]['author']
            break
```

